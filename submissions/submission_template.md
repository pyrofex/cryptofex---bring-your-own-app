# {Your Project Name}

### Full Name

<!---
Example:
Bob Jones
-->

### Photo

<!--- 

Preferably a Headshot


![photo]({Your Photo URL}.jpg)
-->

### Bio

<!---

Write about yourself. Keep it short (< 500 characters)
-->

### About Project


<!--

Tell us what your project does and why it is cool!

-->

### Project Repo 

<!---

Example: 
https://gitlab.com/... 

(You can use Gitlab, Github, etc)

Make sure you include instructions for using your project in the repository
-->


### Confirmation

- [ ] I have read the [contest rules](../RULES.md) and agree to follow them.

- [ ] I understand ...



